# vtgus\RevIO\AddressesApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAddress**](AddressesApi.md#createAddress) | **POST** /addresses | Create a new address
[**getAddressById**](AddressesApi.md#getAddressById) | **GET** /addresses/{id} | Get a specific location by ID.
[**getAddresses**](AddressesApi.md#getAddresses) | **GET** /addresses | Get a collection of locations.


# **createAddress**
> \vtgus\RevIO\Model\ResponseCreate createAddress($address)

Create a new address

Create a new address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$address = new \vtgus\RevIO\Model\Address(); // \vtgus\RevIO\Model\Address | 

try {
    $result = $apiInstance->createAddress($address);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->createAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | [**\vtgus\RevIO\Model\Address**](../Model/Address.md)|  |

### Return type

[**\vtgus\RevIO\Model\ResponseCreate**](../Model/ResponseCreate.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAddressById**
> \vtgus\RevIO\Model\Address getAddressById($id)

Get a specific location by ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 8.14; // float | 

try {
    $result = $apiInstance->getAddressById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->getAddressById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **float**|  |

### Return type

[**\vtgus\RevIO\Model\Address**](../Model/Address.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAddresses**
> \vtgus\RevIO\Model\Addresses getAddresses($page, $page_size, $address_id, $customer_id, $city, $state_or_province, $postal_code, $line_1, $line_2)

Get a collection of locations.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | The current page number in the collection.
$page_size = 10; // int | The number of items to return in the collection.
$address_id = array(56); // int[] | 
$customer_id = array(56); // int[] | 
$city = array("city_example"); // string[] | 
$state_or_province = array("state_or_province_example"); // string[] | 
$postal_code = array("postal_code_example"); // string[] | 
$line_1 = array("line_1_example"); // string[] | 
$line_2 = array("line_2_example"); // string[] | 

try {
    $result = $apiInstance->getAddresses($page, $page_size, $address_id, $customer_id, $city, $state_or_province, $postal_code, $line_1, $line_2);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->getAddresses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The current page number in the collection. | [optional]
 **page_size** | **int**| The number of items to return in the collection. | [optional] [default to 10]
 **address_id** | [**int[]**](../Model/int.md)|  | [optional]
 **customer_id** | [**int[]**](../Model/int.md)|  | [optional]
 **city** | [**string[]**](../Model/string.md)|  | [optional]
 **state_or_province** | [**string[]**](../Model/string.md)|  | [optional]
 **postal_code** | [**string[]**](../Model/string.md)|  | [optional]
 **line_1** | [**string[]**](../Model/string.md)|  | [optional]
 **line_2** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\vtgus\RevIO\Model\Addresses**](../Model/Addresses.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

