# vtgus\RevIO\ServicesApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getServices**](ServicesApi.md#getServices) | **GET** /services | Search Services
[**getServicesById**](ServicesApi.md#getServicesById) | **GET** /services/{id} | Single Service


# **getServices**
> \vtgus\RevIO\Model\Services getServices($page, $page_size, $service_id, $customer_id, $provider_account_number, $number)

Search Services

Allows for limited searching of services in the Rev.io instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\ServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | The current page number in the collection.
$page_size = 10; // int | The number of items to return in the collection.
$service_id = array(56); // int[] | 
$customer_id = array(56); // int[] | 
$provider_account_number = array("provider_account_number_example"); // string[] | 
$number = array("number_example"); // string[] | 

try {
    $result = $apiInstance->getServices($page, $page_size, $service_id, $customer_id, $provider_account_number, $number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServicesApi->getServices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The current page number in the collection. | [optional]
 **page_size** | **int**| The number of items to return in the collection. | [optional] [default to 10]
 **service_id** | [**int[]**](../Model/int.md)|  | [optional]
 **customer_id** | [**int[]**](../Model/int.md)|  | [optional]
 **provider_account_number** | [**string[]**](../Model/string.md)|  | [optional]
 **number** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\vtgus\RevIO\Model\Services**](../Model/Services.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getServicesById**
> \vtgus\RevIO\Model\Service getServicesById($id)

Single Service

Retrieves a single service object by ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\ServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->getServicesById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServicesApi->getServicesById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\vtgus\RevIO\Model\Service**](../Model/Service.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

