# vtgus\RevIO\RequestsApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**approveRequest**](RequestsApi.md#approveRequest) | **PUT** /requests/{id}/approval | Approve a request
[**createRequest**](RequestsApi.md#createRequest) | **POST** /requests | Create a Request
[**downloadRequestPDF**](RequestsApi.md#downloadRequestPDF) | **GET** /requests/{id}/pdf | Download request PDF
[**updateRequest**](RequestsApi.md#updateRequest) | **PATCH** /requests/{id} | Partial update of a request


# **approveRequest**
> object approveRequest($id, $request)

Approve a request

Approve a request

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 
$request = new \vtgus\RevIO\Model\RequestApprove(); // \vtgus\RevIO\Model\RequestApprove | 

try {
    $result = $apiInstance->approveRequest($id, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestsApi->approveRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **request** | [**\vtgus\RevIO\Model\RequestApprove**](../Model/RequestApprove.md)|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createRequest**
> \vtgus\RevIO\Model\ResponseCreate createRequest($request)

Create a Request

Create a new Request

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request = new \vtgus\RevIO\Model\Request(); // \vtgus\RevIO\Model\Request | 

try {
    $result = $apiInstance->createRequest($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestsApi->createRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\vtgus\RevIO\Model\Request**](../Model/Request.md)|  |

### Return type

[**\vtgus\RevIO\Model\ResponseCreate**](../Model/ResponseCreate.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadRequestPDF**
> \SplFileObject downloadRequestPDF($id, $request_id, $request_template_id, $special_message, $attach_pdf_to_request)

Download request PDF

Generate and download a PDF of a request. The request template you specify determines the format of the PDF.  This can be used to create quotes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 
$request_id = 56; // int | 
$request_template_id = 56; // int | 
$special_message = "special_message_example"; // string | 
$attach_pdf_to_request = true; // bool | 

try {
    $result = $apiInstance->downloadRequestPDF($id, $request_id, $request_template_id, $special_message, $attach_pdf_to_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestsApi->downloadRequestPDF: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **request_id** | **int**|  | [optional]
 **request_template_id** | **int**|  | [optional]
 **special_message** | **string**|  | [optional]
 **attach_pdf_to_request** | **bool**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRequest**
> object updateRequest($id, $request)

Partial update of a request

Updates an existing request object in the Rev.io instance to change only the specific fields you indicate.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 
$request = new \vtgus\RevIO\Model\PatchRequest(); // \vtgus\RevIO\Model\PatchRequest | 

try {
    $result = $apiInstance->updateRequest($id, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestsApi->updateRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **request** | [**\vtgus\RevIO\Model\PatchRequest**](../Model/PatchRequest.md)|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json-patch+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

