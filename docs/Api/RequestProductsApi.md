# vtgus\RevIO\RequestProductsApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRequestProduct**](RequestProductsApi.md#createRequestProduct) | **POST** /requestproducts | Create a request product
[**deleteRequestProduct**](RequestProductsApi.md#deleteRequestProduct) | **DELETE** /requestproducts/{id} | Delete a request product
[**updateRequestProduct**](RequestProductsApi.md#updateRequestProduct) | **PATCH** /requestproducts/{id} | Partial update of a request products


# **createRequestProduct**
> \vtgus\RevIO\Model\ResponseCreate createRequestProduct($request)

Create a request product

Create a request product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request = new \vtgus\RevIO\Model\RequestProduct(); // \vtgus\RevIO\Model\RequestProduct | 

try {
    $result = $apiInstance->createRequestProduct($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestProductsApi->createRequestProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\vtgus\RevIO\Model\RequestProduct**](../Model/RequestProduct.md)|  |

### Return type

[**\vtgus\RevIO\Model\ResponseCreate**](../Model/ResponseCreate.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRequestProduct**
> object deleteRequestProduct($id)

Delete a request product

Delete a request product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 

try {
    $result = $apiInstance->deleteRequestProduct($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestProductsApi->deleteRequestProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRequestProduct**
> object updateRequestProduct($id, $request)

Partial update of a request products

Updates an existing request object in the Rev.io instance to change only the specific fields you indicate.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 
$request = new \vtgus\RevIO\Model\PatchRequest(); // \vtgus\RevIO\Model\PatchRequest | 

try {
    $result = $apiInstance->updateRequestProduct($id, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestProductsApi->updateRequestProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **request** | [**\vtgus\RevIO\Model\PatchRequest**](../Model/PatchRequest.md)|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json-patch+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

