# vtgus\RevIO\RequestServicesApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRequestService**](RequestServicesApi.md#createRequestService) | **POST** /requestservices | Create a request service
[**deleteRequestService**](RequestServicesApi.md#deleteRequestService) | **DELETE** /requestservices/{id} | Delete a request service
[**updateRequestService**](RequestServicesApi.md#updateRequestService) | **PATCH** /requestservices/{id} | Partial update of a request services


# **createRequestService**
> \vtgus\RevIO\Model\ResponseCreate createRequestService($request)

Create a request service

Create a new request service.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request = new \vtgus\RevIO\Model\RequestService(); // \vtgus\RevIO\Model\RequestService | 

try {
    $result = $apiInstance->createRequestService($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestServicesApi->createRequestService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\vtgus\RevIO\Model\RequestService**](../Model/RequestService.md)|  |

### Return type

[**\vtgus\RevIO\Model\ResponseCreate**](../Model/ResponseCreate.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRequestService**
> object deleteRequestService($id)

Delete a request service

Delete a request service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 

try {
    $result = $apiInstance->deleteRequestService($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestServicesApi->deleteRequestService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRequestService**
> object updateRequestService($id, $request)

Partial update of a request services

Updates an existing request object in the Rev.io instance to change only the specific fields you indicate.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 
$request = new \vtgus\RevIO\Model\PatchRequest(); // \vtgus\RevIO\Model\PatchRequest | 

try {
    $result = $apiInstance->updateRequestService($id, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestServicesApi->updateRequestService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **request** | [**\vtgus\RevIO\Model\PatchRequest**](../Model/PatchRequest.md)|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json-patch+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

