# vtgus\RevIO\NotesApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createNote**](NotesApi.md#createNote) | **POST** /notes | Create a new note


# **createNote**
> \vtgus\RevIO\Model\ResponseCreate createNote($note)

Create a new note

Create a new note

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\NotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$note = new \vtgus\RevIO\Model\Note(); // \vtgus\RevIO\Model\Note | 

try {
    $result = $apiInstance->createNote($note);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotesApi->createNote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **note** | [**\vtgus\RevIO\Model\Note**](../Model/Note.md)|  |

### Return type

[**\vtgus\RevIO\Model\ResponseCreate**](../Model/ResponseCreate.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

