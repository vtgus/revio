# vtgus\RevIO\ProductsApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductById**](ProductsApi.md#getProductById) | **GET** /products/{id} | Get Product by ID
[**getProducts**](ProductsApi.md#getProducts) | **GET** /products | Get Products


# **getProductById**
> \vtgus\RevIO\Model\Product getProductById($id)

Get Product by ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->getProductById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProductById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\vtgus\RevIO\Model\Product**](../Model/Product.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProducts**
> \vtgus\RevIO\Model\Products getProducts($page, $page_size, $product_id, $provider_id, $active, $product_type_id, $description, $code_1, $created_date_start, $created_date_end)

Get Products

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | The current page number in the collection.
$page_size = 10; // int | The number of items to return in the collection.
$product_id = array(56); // int[] | 
$provider_id = array(56); // int[] | 
$active = true; // bool | 
$product_type_id = array(56); // int[] | 
$description = array("description_example"); // string[] | 
$code_1 = array("code_1_example"); // string[] | 
$created_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$created_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 

try {
    $result = $apiInstance->getProducts($page, $page_size, $product_id, $provider_id, $active, $product_type_id, $description, $code_1, $created_date_start, $created_date_end);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The current page number in the collection. | [optional]
 **page_size** | **int**| The number of items to return in the collection. | [optional] [default to 10]
 **product_id** | [**int[]**](../Model/int.md)|  | [optional]
 **provider_id** | [**int[]**](../Model/int.md)|  | [optional]
 **active** | **bool**|  | [optional]
 **product_type_id** | [**int[]**](../Model/int.md)|  | [optional]
 **description** | [**string[]**](../Model/string.md)|  | [optional]
 **code_1** | [**string[]**](../Model/string.md)|  | [optional]
 **created_date_start** | **\DateTime**|  | [optional]
 **created_date_end** | **\DateTime**|  | [optional]

### Return type

[**\vtgus\RevIO\Model\Products**](../Model/Products.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

