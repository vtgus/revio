# vtgus\RevIO\ServiceProductsApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getServiceProducts**](ServiceProductsApi.md#getServiceProducts) | **GET** /serviceproduct | Search service products


# **getServiceProducts**
> \vtgus\RevIO\Model\ServiceProducts getServiceProducts($page, $page_size, $service_id, $customer_id, $product_id, $package_id, $description, $code_1, $status, $billed_through_date_start, $billed_through_date_end, $activation_date_start, $activation_date_end, $canceled_date_start, $canceled_date_end, $created_date_start, $created_date_end, $status_date_start, $status_date_end, $contract_start_date_start, $contract_start_date_end, $contract_end_date_start, $contract_end_date_end)

Search service products

Search for one or more service products.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\ServiceProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | The current page number in the collection.
$page_size = 10; // int | The number of items to return in the collection.
$service_id = array(56); // int[] | 
$customer_id = array(56); // int[] | 
$product_id = array(56); // int[] | 
$package_id = array(56); // int[] | 
$description = array("description_example"); // string[] | 
$code_1 = array("code_1_example"); // string[] | 
$status = array("status_example"); // string[] | 
$billed_through_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$billed_through_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$activation_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$activation_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$canceled_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$canceled_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$created_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$created_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$status_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$status_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$contract_start_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$contract_start_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$contract_end_date_start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$contract_end_date_end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 

try {
    $result = $apiInstance->getServiceProducts($page, $page_size, $service_id, $customer_id, $product_id, $package_id, $description, $code_1, $status, $billed_through_date_start, $billed_through_date_end, $activation_date_start, $activation_date_end, $canceled_date_start, $canceled_date_end, $created_date_start, $created_date_end, $status_date_start, $status_date_end, $contract_start_date_start, $contract_start_date_end, $contract_end_date_start, $contract_end_date_end);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceProductsApi->getServiceProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The current page number in the collection. | [optional]
 **page_size** | **int**| The number of items to return in the collection. | [optional] [default to 10]
 **service_id** | [**int[]**](../Model/int.md)|  | [optional]
 **customer_id** | [**int[]**](../Model/int.md)|  | [optional]
 **product_id** | [**int[]**](../Model/int.md)|  | [optional]
 **package_id** | [**int[]**](../Model/int.md)|  | [optional]
 **description** | [**string[]**](../Model/string.md)|  | [optional]
 **code_1** | [**string[]**](../Model/string.md)|  | [optional]
 **status** | [**string[]**](../Model/string.md)|  | [optional]
 **billed_through_date_start** | **\DateTime**|  | [optional]
 **billed_through_date_end** | **\DateTime**|  | [optional]
 **activation_date_start** | **\DateTime**|  | [optional]
 **activation_date_end** | **\DateTime**|  | [optional]
 **canceled_date_start** | **\DateTime**|  | [optional]
 **canceled_date_end** | **\DateTime**|  | [optional]
 **created_date_start** | **\DateTime**|  | [optional]
 **created_date_end** | **\DateTime**|  | [optional]
 **status_date_start** | **\DateTime**|  | [optional]
 **status_date_end** | **\DateTime**|  | [optional]
 **contract_start_date_start** | **\DateTime**|  | [optional]
 **contract_start_date_end** | **\DateTime**|  | [optional]
 **contract_end_date_start** | **\DateTime**|  | [optional]
 **contract_end_date_end** | **\DateTime**|  | [optional]

### Return type

[**\vtgus\RevIO\Model\ServiceProducts**](../Model/ServiceProducts.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

