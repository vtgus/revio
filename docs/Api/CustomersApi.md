# vtgus\RevIO\CustomersApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomer**](CustomersApi.md#createCustomer) | **POST** /customers | Create a new customer
[**getCustomerById**](CustomersApi.md#getCustomerById) | **GET** /customers/{id} | Get a specific customer by Rev.io ID.
[**getCustomers**](CustomersApi.md#getCustomers) | **GET** /customers | Search Customers


# **createCustomer**
> \vtgus\RevIO\Model\ResponseCreate createCustomer($customer)

Create a new customer

Create a new customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer = new \vtgus\RevIO\Model\Customer(); // \vtgus\RevIO\Model\Customer | 

try {
    $result = $apiInstance->createCustomer($customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->createCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer** | [**\vtgus\RevIO\Model\Customer**](../Model/Customer.md)|  |

### Return type

[**\vtgus\RevIO\Model\ResponseCreate**](../Model/ResponseCreate.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCustomerById**
> \vtgus\RevIO\Model\Customer getCustomerById($id)

Get a specific customer by Rev.io ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->getCustomerById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getCustomerById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\vtgus\RevIO\Model\Customer**](../Model/Customer.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCustomers**
> \vtgus\RevIO\Model\Customers getCustomers($page, $page_size, $customer_id, $account_number, $parent_customer_id, $is_parent, $bill_profile_id, $status, $name)

Search Customers

Allows for limited searching of customers in the Rev.io instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | The current page number in the collection.
$page_size = 10; // int | The number of items to return in the collection.
$customer_id = array(56); // int[] | 
$account_number = array("account_number_example"); // string[] | 
$parent_customer_id = array(56); // int[] | 
$is_parent = true; // bool | 
$bill_profile_id = array(56); // int[] | 
$status = "status_example"; // string | 
$name = "name_example"; // string | 

try {
    $result = $apiInstance->getCustomers($page, $page_size, $customer_id, $account_number, $parent_customer_id, $is_parent, $bill_profile_id, $status, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getCustomers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The current page number in the collection. | [optional]
 **page_size** | **int**| The number of items to return in the collection. | [optional] [default to 10]
 **customer_id** | [**int[]**](../Model/int.md)|  | [optional]
 **account_number** | [**string[]**](../Model/string.md)|  | [optional]
 **parent_customer_id** | [**int[]**](../Model/int.md)|  | [optional]
 **is_parent** | **bool**|  | [optional]
 **bill_profile_id** | [**int[]**](../Model/int.md)|  | [optional]
 **status** | **string**|  | [optional]
 **name** | **string**|  | [optional]

### Return type

[**\vtgus\RevIO\Model\Customers**](../Model/Customers.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

