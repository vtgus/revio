# vtgus\RevIO\RequestAddressesApi

All URIs are relative to *https://restapi.rev.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRequestAddress**](RequestAddressesApi.md#createRequestAddress) | **POST** /requestaddresses | Create a request address
[**deleteRequestAddress**](RequestAddressesApi.md#deleteRequestAddress) | **DELETE** /requestaddresses/{id} | Delete a request address
[**updateRequestAddress**](RequestAddressesApi.md#updateRequestAddress) | **PATCH** /requestaddresses/{id} | Partial update of a request address


# **createRequestAddress**
> \vtgus\RevIO\Model\ResponseCreate createRequestAddress($request)

Create a request address

Create a request address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestAddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request = new \vtgus\RevIO\Model\RequestAddress(); // \vtgus\RevIO\Model\RequestAddress | 

try {
    $result = $apiInstance->createRequestAddress($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestAddressesApi->createRequestAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\vtgus\RevIO\Model\RequestAddress**](../Model/RequestAddress.md)|  |

### Return type

[**\vtgus\RevIO\Model\ResponseCreate**](../Model/ResponseCreate.md)

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRequestAddress**
> object deleteRequestAddress($id)

Delete a request address

Delete a request address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestAddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 

try {
    $result = $apiInstance->deleteRequestAddress($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestAddressesApi->deleteRequestAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRequestAddress**
> object updateRequestAddress($id, $request)

Partial update of a request address

Updates an existing request object in the Rev.io instance to change only the specific fields you indicate.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new vtgus\RevIO\Api\RequestAddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 
$request = new \vtgus\RevIO\Model\PatchRequest(); // \vtgus\RevIO\Model\PatchRequest | 

try {
    $result = $apiInstance->updateRequestAddress($id, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequestAddressesApi->updateRequestAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **request** | [**\vtgus\RevIO\Model\PatchRequest**](../Model/PatchRequest.md)|  |

### Return type

**object**

### Authorization

[Basic authentication](../../README.md#Basic authentication)

### HTTP request headers

 - **Content-Type**: application/json-patch+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

