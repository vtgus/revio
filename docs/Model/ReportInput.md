# ReportInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | **string** |  | 
**clear_cache** | **bool** |  | [optional] 
**prompts** | [**\vtgus\RevIO\Model\ReportPrompts[]**](ReportPrompts.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


