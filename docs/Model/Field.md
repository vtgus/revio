# Field

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field_id** | **int** |  | [optional] 
**label** | **string** |  | [optional] 
**value** | **string** |  | [optional] 
**links** | [**\vtgus\RevIO\Model\Link[]**](Link.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


