# ReportPrompts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prompt_id** | **int** |  | [optional] 
**column** | **string** |  | [optional] 
**value** | **string** |  | [optional] 
**operator** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


