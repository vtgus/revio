# RequestAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**request_address_id** | **int** |  | [optional] 
**request_id** | **int** |  | [optional] 
**address_id** | **int** |  | [optional] 
**is_shipping_address** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


