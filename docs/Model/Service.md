# Service

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**service_id** | **float** |  | [optional] 
**customer_id** | **float** |  | [optional] 
**provider_id** | **float** |  | [optional] 
**service_type_id** | **float** |  | [optional] 
**package_id** | **float** |  | [optional] 
**number** | **string** |  | [optional] 
**pic** | **string** |  | [optional] 
**lpic** | **string** |  | [optional] 
**provider_account_id** | **float** |  | [optional] 
**usage_plan_group_id** | **float** |  | [optional] 
**number2** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**disconnected_date** | **string** |  | [optional] 
**activated_date** | **string** |  | [optional] 
**address_id** | **float** |  | [optional] 
**addressz_id** | **float** |  | [optional] 
**provider_account_number** | **string** |  | [optional] 
**contract_start** | **string** |  | [optional] 
**contract_end** | **string** |  | [optional] 
**created_date** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


