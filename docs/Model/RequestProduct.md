# RequestProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**request_product_id** | **int** |  | [optional] 
**request_service_id** | **int** |  | 
**request_id** | **int** |  | 
**product_id** | **int** |  | 
**quantity** | **int** |  | 
**rate** | **double** |  | [optional] 
**description** | **string** |  | [optional] 
**code_1** | **string** |  | [optional] 
**code_2** | **string** |  | [optional] 
**contract_start** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contract_end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**group_on_bill** | **bool** |  | [optional] 
**tax_included** | **bool** |  | [optional] 
**itemized** | **bool** |  | [optional] 
**cost** | **double** |  | [optional] 
**buy_rate** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


