# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**customer_id** | **float** |  | [optional] 
**parent_customer_id** | **float** |  | [optional] 
**account_number** | **string** |  | [optional] 
**activated_date** | **string** |  | [optional] 
**agent_id** | **float** |  | [optional] 
**close_date** | **string** |  | [optional] 
**created_by** | **float** |  | [optional] 
**created_date** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**is_parent** | **bool** |  | [optional] 
**registration_code** | **string** |  | [optional] 
**security_pin** | **string** |  | [optional] 
**sold_by** | **float** |  | [optional] 
**source** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**auto_debit** | [**\vtgus\RevIO\Model\AutoDebit**](AutoDebit.md) |  | [optional] 
**finance** | [**\vtgus\RevIO\Model\Finance**](Finance.md) |  | [optional] 
**profile** | [**\vtgus\RevIO\Model\Profile**](Profile.md) |  | [optional] 
**billing_address** | [**\vtgus\RevIO\Model\Address**](Address.md) |  | [optional] 
**service_address** | [**\vtgus\RevIO\Model\Address**](Address.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


