# Note

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**note_id** | **int** |  | [optional] 
**subject** | **string** |  | [optional] 
**body** | **string** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_by** | **int** |  | [optional] 
**customer_id** | **int** |  | [optional] 
**order_id** | **int** |  | [optional] 
**request_id** | **int** |  | [optional] 
**task_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


