# Products

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**has_more** | **bool** |  | [optional] 
**record_count** | **float** |  | [optional] 
**records** | [**\vtgus\RevIO\Model\Product[]**](Product.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


