# Request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**request_id** | **int** |  | [optional] 
**request_status_id** | **int** |  | [optional] 
**customer_id** | **int** |  | [optional] 
**assigned_to** | **int** |  | [optional] 
**process_id** | **int** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_by** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


