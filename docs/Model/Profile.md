# Profile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**class** | **string** |  | [optional] 
**date_of_birth** | **string** |  | [optional] 
**language** | **string** |  | [optional] 
**tax_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


