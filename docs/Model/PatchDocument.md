# PatchDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**op** | **string** | The operation to be performed | 
**path** | **string** | A JSON-Pointer | 
**value** | **string** | The value to be used within the operations. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


