# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **int** |  | [optional] 
**product_type_id** | **int** |  | [optional] 
**description** | **string** |  | [optional] 
**code_1** | **string** |  | [optional] 
**code_2** | **string** |  | [optional] 
**rate** | **double** |  | [optional] 
**cost** | **double** |  | [optional] 
**buy_rate** | **double** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_by** | **int** |  | [optional] 
**active** | **bool** |  | [optional] 
**creates_order** | **bool** |  | [optional] 
**provider_id** | **int** |  | [optional] 
**bills_in_arrears** | **bool** |  | [optional] 
**prorates** | **bool** |  | [optional] 
**customer_class** | **string** |  | [optional] 
**long_description** | **string** |  | [optional] 
**ledger_code** | **string** |  | [optional] 
**free_months** | **int** |  | [optional] 
**automatic_expiration_months** | **int** |  | [optional] 
**order_completion_billing** | **bool** |  | [optional] 
**tax_class_id** | **int** |  | [optional] 
**wholesale_description** | **string** |  | [optional] 
**fields** | [**\vtgus\RevIO\Model\Field[]**](Field.md) |  | [optional] 
**links** | [**\vtgus\RevIO\Model\Link[]**](Link.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


