# RequestService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**request_service_id** | **int** |  | [optional] 
**request_address_id** | **int** |  | 
**request_addressz_id** | **int** |  | [optional] 
**request_id** | **int** |  | 
**service_type_id** | **int** |  | 
**provider_id** | **int** |  | 
**provider_account_id** | **int** |  | [optional] 
**usage_plan_group_id** | **int** |  | [optional] 
**package_id** | **int** |  | [optional] 
**number** | **string** |  | [optional] 
**number2** | **string** |  | [optional] 
**provider_account_number** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_by** | **int** |  | [optional] 
**contract_start** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contract_end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**pic** | **string** |  | [optional] 
**lpic** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


