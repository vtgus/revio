# Finance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount_due** | **float** |  | [optional] 
**amount_overdue** | **float** |  | [optional] 
**bill_profile_id** | **float** |  | [optional] 
**billing_method** | **string** |  | [optional] 
**balance** | **float** |  | [optional] 
**balance_limit** | **float** |  | [optional] 
**balance_limit_enabled** | **bool** |  | [optional] 
**due_date** | **string** |  | [optional] 
**collection_template_id** | **float** |  | [optional] 
**contract_end_date** | **string** |  | [optional] 
**contract_start_date** | **string** |  | [optional] 
**cycle_date** | **string** |  | [optional] 
**late_fees_enabled** | **bool** |  | [optional] 
**payment_terms_days** | **float** |  | [optional] 
**tax_exempt_enabled** | **bool** |  | [optional] 
**tax_exempt_types** | **string** |  | [optional] 
**usage_file_enabled** | **bool** |  | [optional] 
**usage_file_format** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


