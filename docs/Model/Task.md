# Task

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**task_id** | **int** |  | [optional] 
**subject** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**complete** | **bool** |  | [optional] 
**canceled** | **bool** |  | [optional] 
**assigned_to** | **int** |  | [optional] 
**group_assigned_to** | **int** |  | [optional] 
**task_type_id** | **int** |  | [optional] 
**customer_id** | **int** |  | [optional] 
**request_id** | **int** |  | [optional] 
**order_id** | **int** |  | [optional] 
**task_step_id** | **int** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**fields** | [**\vtgus\RevIO\Model\Field[]**](Field.md) |  | [optional] 
**links** | [**\vtgus\RevIO\Model\Link[]**](Link.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


