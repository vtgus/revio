# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **int** |  | [optional] 
**pon** | **string** |  | [optional] 
**customer_id** | **int** |  | [optional] 
**order_type** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**provider_id** | **int** |  | [optional] 
**assigned_to** | **int** |  | [optional] 
**request_id** | **int** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**completed_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**fields** | [**\vtgus\RevIO\Model\Field[]**](Field.md) |  | [optional] 
**links** | [**\vtgus\RevIO\Model\Link[]**](Link.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


