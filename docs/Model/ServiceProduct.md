# ServiceProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_product_id** | **int** |  | [optional] 
**customer_id** | **int** |  | [optional] 
**product_id** | **int** |  | [optional] 
**package_id** | **int** |  | [optional] 
**service_id** | **int** |  | [optional] 
**description** | **string** |  | [optional] 
**code_1** | **string** |  | [optional] 
**code_2** | **string** |  | [optional] 
**rate** | **double** |  | [optional] 
**billed_through_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**canceled_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**status** | **string** |  | [optional] 
**status_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**status_user_id** | **int** |  | [optional] 
**activated_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**cost** | **double** |  | [optional] 
**wholesale_description** | **string** |  | [optional] 
**free_start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**free_end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**quantity** | **int** |  | [optional] 
**contract_start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contract_end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**tax_included** | **bool** |  | [optional] 
**group_on_bill** | **bool** |  | [optional] 
**itemized** | **bool** |  | [optional] 
**links** | [**\vtgus\RevIO\Model\Link[]**](Link.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


