# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **bool** |  | [optional] 
**address_id** | **float** |  | [optional] 
**customer_id** | **float** |  | [optional] 
**first_name** | **string** |  | [optional] 
**middle_initial** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**company_name** | **string** |  | [optional] 
**line_1** | **string** |  | [optional] 
**line_2** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**state_or_province** | **string** |  | [optional] 
**postal_code** | **string** |  | [optional] 
**postal_code_extension** | **string** |  | [optional] 
**country_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


