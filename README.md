# SwaggerClient-php
New Rest API for Rev.IO

This PHP package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: io.swagger.codegen.languages.PhpClientCodegen

## Requirements

PHP 5.5 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://bitbucket.org/vtgus/RevIO.git"
    }
  ],
  "require": {
    "vtgus/RevIO": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/SwaggerClient-php/vendor/autoload.php');
```

## Tests

To run the unit tests:

```
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic authentication
$config = vtgus\RevIO\Configuration::getDefaultConfiguration()
    ->setUsername('YOUR_USERNAME')
    ->setPassword('YOUR_PASSWORD');

$apiInstance = new vtgus\RevIO\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$address = new \vtgus\RevIO\Model\Address(); // \vtgus\RevIO\Model\Address | 

try {
    $result = $apiInstance->createAddress($address);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->createAddress: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *https://restapi.rev.io/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AddressesApi* | [**createAddress**](docs/Api/AddressesApi.md#createaddress) | **POST** /addresses | Create a new address
*AddressesApi* | [**getAddressById**](docs/Api/AddressesApi.md#getaddressbyid) | **GET** /addresses/{id} | Get a specific location by ID.
*AddressesApi* | [**getAddresses**](docs/Api/AddressesApi.md#getaddresses) | **GET** /addresses | Get a collection of locations.
*CustomersApi* | [**createCustomer**](docs/Api/CustomersApi.md#createcustomer) | **POST** /customers | Create a new customer
*CustomersApi* | [**getCustomerById**](docs/Api/CustomersApi.md#getcustomerbyid) | **GET** /customers/{id} | Get a specific customer by Rev.io ID.
*CustomersApi* | [**getCustomers**](docs/Api/CustomersApi.md#getcustomers) | **GET** /customers | Search Customers
*NotesApi* | [**createNote**](docs/Api/NotesApi.md#createnote) | **POST** /notes | Create a new note
*OrdersApi* | [**getOrderById**](docs/Api/OrdersApi.md#getorderbyid) | **GET** /orders/{id} | Get Order by ID
*OrdersApi* | [**updateOrder**](docs/Api/OrdersApi.md#updateorder) | **PATCH** /orders/{id} | Partial update of a Order
*ProductsApi* | [**getProductById**](docs/Api/ProductsApi.md#getproductbyid) | **GET** /products/{id} | Get Product by ID
*ProductsApi* | [**getProducts**](docs/Api/ProductsApi.md#getproducts) | **GET** /products | Get Products
*ReportsApi* | [**executeReport**](docs/Api/ReportsApi.md#executereport) | **GET** /reports/{id}/results | Execute Report
*ReportsApi* | [**executeReportInput**](docs/Api/ReportsApi.md#executereportinput) | **POST** /reports/{id}/results | Execute Report with Input
*RequestAddressesApi* | [**createRequestAddress**](docs/Api/RequestAddressesApi.md#createrequestaddress) | **POST** /requestaddresses | Create a request address
*RequestAddressesApi* | [**deleteRequestAddress**](docs/Api/RequestAddressesApi.md#deleterequestaddress) | **DELETE** /requestaddresses/{id} | Delete a request address
*RequestAddressesApi* | [**updateRequestAddress**](docs/Api/RequestAddressesApi.md#updaterequestaddress) | **PATCH** /requestaddresses/{id} | Partial update of a request address
*RequestProductsApi* | [**createRequestProduct**](docs/Api/RequestProductsApi.md#createrequestproduct) | **POST** /requestproducts | Create a request product
*RequestProductsApi* | [**deleteRequestProduct**](docs/Api/RequestProductsApi.md#deleterequestproduct) | **DELETE** /requestproducts/{id} | Delete a request product
*RequestProductsApi* | [**updateRequestProduct**](docs/Api/RequestProductsApi.md#updaterequestproduct) | **PATCH** /requestproducts/{id} | Partial update of a request products
*RequestServicesApi* | [**createRequestService**](docs/Api/RequestServicesApi.md#createrequestservice) | **POST** /requestservices | Create a request service
*RequestServicesApi* | [**deleteRequestService**](docs/Api/RequestServicesApi.md#deleterequestservice) | **DELETE** /requestservices/{id} | Delete a request service
*RequestServicesApi* | [**updateRequestService**](docs/Api/RequestServicesApi.md#updaterequestservice) | **PATCH** /requestservices/{id} | Partial update of a request services
*RequestsApi* | [**approveRequest**](docs/Api/RequestsApi.md#approverequest) | **PUT** /requests/{id}/approval | Approve a request
*RequestsApi* | [**createRequest**](docs/Api/RequestsApi.md#createrequest) | **POST** /requests | Create a Request
*RequestsApi* | [**downloadRequestPDF**](docs/Api/RequestsApi.md#downloadrequestpdf) | **GET** /requests/{id}/pdf | Download request PDF
*RequestsApi* | [**updateRequest**](docs/Api/RequestsApi.md#updaterequest) | **PATCH** /requests/{id} | Partial update of a request
*ServiceProductsApi* | [**getServiceProducts**](docs/Api/ServiceProductsApi.md#getserviceproducts) | **GET** /serviceproduct | Search service products
*ServicesApi* | [**getServices**](docs/Api/ServicesApi.md#getservices) | **GET** /services | Search Services
*ServicesApi* | [**getServicesById**](docs/Api/ServicesApi.md#getservicesbyid) | **GET** /services/{id} | Single Service
*TasksApi* | [**getTaskById**](docs/Api/TasksApi.md#gettaskbyid) | **GET** /tasks/{id} | Find a task by ID


## Documentation For Models

 - [Address](docs/Model/Address.md)
 - [Addresses](docs/Model/Addresses.md)
 - [AutoDebit](docs/Model/AutoDebit.md)
 - [Customer](docs/Model/Customer.md)
 - [Customers](docs/Model/Customers.md)
 - [Field](docs/Model/Field.md)
 - [Finance](docs/Model/Finance.md)
 - [Link](docs/Model/Link.md)
 - [Note](docs/Model/Note.md)
 - [Order](docs/Model/Order.md)
 - [PatchDocument](docs/Model/PatchDocument.md)
 - [PatchRequest](docs/Model/PatchRequest.md)
 - [Product](docs/Model/Product.md)
 - [Products](docs/Model/Products.md)
 - [Profile](docs/Model/Profile.md)
 - [ReportInput](docs/Model/ReportInput.md)
 - [ReportPrompts](docs/Model/ReportPrompts.md)
 - [ReportResults](docs/Model/ReportResults.md)
 - [Request](docs/Model/Request.md)
 - [RequestAddress](docs/Model/RequestAddress.md)
 - [RequestApprove](docs/Model/RequestApprove.md)
 - [RequestProduct](docs/Model/RequestProduct.md)
 - [RequestService](docs/Model/RequestService.md)
 - [ResponseCreate](docs/Model/ResponseCreate.md)
 - [Service](docs/Model/Service.md)
 - [ServiceProduct](docs/Model/ServiceProduct.md)
 - [ServiceProducts](docs/Model/ServiceProducts.md)
 - [Services](docs/Model/Services.md)
 - [Task](docs/Model/Task.md)


## Documentation For Authorization


## Basic authentication

- **Type**: HTTP basic authentication


## Author




