<?php
/**
 * CustomerTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  vtgus\RevIO
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * RevIO
 *
 * New Rest API for Rev.IO
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace vtgus\RevIO;

/**
 * CustomerTest Class Doc Comment
 *
 * @category    Class
 * @description Customer
 * @package     vtgus\RevIO
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CustomerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Customer"
     */
    public function testCustomer()
    {
    }

    /**
     * Test attribute "ok"
     */
    public function testPropertyOk()
    {
    }

    /**
     * Test attribute "customer_id"
     */
    public function testPropertyCustomerId()
    {
    }

    /**
     * Test attribute "parent_customer_id"
     */
    public function testPropertyParentCustomerId()
    {
    }

    /**
     * Test attribute "account_number"
     */
    public function testPropertyAccountNumber()
    {
    }

    /**
     * Test attribute "activated_date"
     */
    public function testPropertyActivatedDate()
    {
    }

    /**
     * Test attribute "agent_id"
     */
    public function testPropertyAgentId()
    {
    }

    /**
     * Test attribute "close_date"
     */
    public function testPropertyCloseDate()
    {
    }

    /**
     * Test attribute "created_by"
     */
    public function testPropertyCreatedBy()
    {
    }

    /**
     * Test attribute "created_date"
     */
    public function testPropertyCreatedDate()
    {
    }

    /**
     * Test attribute "email"
     */
    public function testPropertyEmail()
    {
    }

    /**
     * Test attribute "is_parent"
     */
    public function testPropertyIsParent()
    {
    }

    /**
     * Test attribute "registration_code"
     */
    public function testPropertyRegistrationCode()
    {
    }

    /**
     * Test attribute "security_pin"
     */
    public function testPropertySecurityPin()
    {
    }

    /**
     * Test attribute "sold_by"
     */
    public function testPropertySoldBy()
    {
    }

    /**
     * Test attribute "source"
     */
    public function testPropertySource()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "auto_debit"
     */
    public function testPropertyAutoDebit()
    {
    }

    /**
     * Test attribute "finance"
     */
    public function testPropertyFinance()
    {
    }

    /**
     * Test attribute "profile"
     */
    public function testPropertyProfile()
    {
    }

    /**
     * Test attribute "billing_address"
     */
    public function testPropertyBillingAddress()
    {
    }

    /**
     * Test attribute "service_address"
     */
    public function testPropertyServiceAddress()
    {
    }
}
