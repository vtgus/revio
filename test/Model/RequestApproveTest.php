<?php
/**
 * RequestApproveTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  vtgus\RevIO
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * RevIO
 *
 * New Rest API for Rev.IO
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace vtgus\RevIO;

/**
 * RequestApproveTest Class Doc Comment
 *
 * @category    Class
 * @description RequestApprove
 * @package     vtgus\RevIO
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RequestApproveTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "RequestApprove"
     */
    public function testRequestApprove()
    {
    }

    /**
     * Test attribute "ok"
     */
    public function testPropertyOk()
    {
    }

    /**
     * Test attribute "method"
     */
    public function testPropertyMethod()
    {
    }
}
