<?php
/**
 * ReportPromptsTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  vtgus\RevIO
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * RevIO
 *
 * New Rest API for Rev.IO
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace vtgus\RevIO;

/**
 * ReportPromptsTest Class Doc Comment
 *
 * @category    Class
 * @description ReportPrompts
 * @package     vtgus\RevIO
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ReportPromptsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ReportPrompts"
     */
    public function testReportPrompts()
    {
    }

    /**
     * Test attribute "prompt_id"
     */
    public function testPropertyPromptId()
    {
    }

    /**
     * Test attribute "column"
     */
    public function testPropertyColumn()
    {
    }

    /**
     * Test attribute "value"
     */
    public function testPropertyValue()
    {
    }

    /**
     * Test attribute "operator"
     */
    public function testPropertyOperator()
    {
    }
}
