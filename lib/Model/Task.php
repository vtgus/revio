<?php
/**
 * Task
 *
 * PHP version 5
 *
 * @category Class
 * @package  vtgus\RevIO
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * RevIO
 *
 * New Rest API for Rev.IO
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace vtgus\RevIO\Model;

use \ArrayAccess;
use \vtgus\RevIO\ObjectSerializer;

/**
 * Task Class Doc Comment
 *
 * @category Class
 * @package  vtgus\RevIO
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Task implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'Task';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'ok' => 'bool',
        'task_id' => 'int',
        'subject' => 'string',
        'message' => 'string',
        'complete' => 'bool',
        'canceled' => 'bool',
        'assigned_to' => 'int',
        'group_assigned_to' => 'int',
        'task_type_id' => 'int',
        'customer_id' => 'int',
        'request_id' => 'int',
        'order_id' => 'int',
        'task_step_id' => 'int',
        'created_date' => '\DateTime',
        'fields' => '\vtgus\RevIO\Model\Field[]',
        'links' => '\vtgus\RevIO\Model\Link[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'ok' => null,
        'task_id' => 'int32',
        'subject' => null,
        'message' => null,
        'complete' => null,
        'canceled' => null,
        'assigned_to' => 'int32',
        'group_assigned_to' => 'int32',
        'task_type_id' => 'int32',
        'customer_id' => 'int32',
        'request_id' => 'int32',
        'order_id' => 'int32',
        'task_step_id' => 'int32',
        'created_date' => 'date-time',
        'fields' => null,
        'links' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'ok' => 'ok',
        'task_id' => 'task_id',
        'subject' => 'subject',
        'message' => 'message',
        'complete' => 'complete',
        'canceled' => 'canceled',
        'assigned_to' => 'assigned_to',
        'group_assigned_to' => 'group_assigned_to',
        'task_type_id' => 'task_type_id',
        'customer_id' => 'customer_id',
        'request_id' => 'request_id',
        'order_id' => 'order_id',
        'task_step_id' => 'task_step_id',
        'created_date' => 'created_date',
        'fields' => 'fields',
        'links' => 'links'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'ok' => 'setOk',
        'task_id' => 'setTaskId',
        'subject' => 'setSubject',
        'message' => 'setMessage',
        'complete' => 'setComplete',
        'canceled' => 'setCanceled',
        'assigned_to' => 'setAssignedTo',
        'group_assigned_to' => 'setGroupAssignedTo',
        'task_type_id' => 'setTaskTypeId',
        'customer_id' => 'setCustomerId',
        'request_id' => 'setRequestId',
        'order_id' => 'setOrderId',
        'task_step_id' => 'setTaskStepId',
        'created_date' => 'setCreatedDate',
        'fields' => 'setFields',
        'links' => 'setLinks'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'ok' => 'getOk',
        'task_id' => 'getTaskId',
        'subject' => 'getSubject',
        'message' => 'getMessage',
        'complete' => 'getComplete',
        'canceled' => 'getCanceled',
        'assigned_to' => 'getAssignedTo',
        'group_assigned_to' => 'getGroupAssignedTo',
        'task_type_id' => 'getTaskTypeId',
        'customer_id' => 'getCustomerId',
        'request_id' => 'getRequestId',
        'order_id' => 'getOrderId',
        'task_step_id' => 'getTaskStepId',
        'created_date' => 'getCreatedDate',
        'fields' => 'getFields',
        'links' => 'getLinks'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['ok'] = isset($data['ok']) ? $data['ok'] : null;
        $this->container['task_id'] = isset($data['task_id']) ? $data['task_id'] : null;
        $this->container['subject'] = isset($data['subject']) ? $data['subject'] : null;
        $this->container['message'] = isset($data['message']) ? $data['message'] : null;
        $this->container['complete'] = isset($data['complete']) ? $data['complete'] : null;
        $this->container['canceled'] = isset($data['canceled']) ? $data['canceled'] : null;
        $this->container['assigned_to'] = isset($data['assigned_to']) ? $data['assigned_to'] : null;
        $this->container['group_assigned_to'] = isset($data['group_assigned_to']) ? $data['group_assigned_to'] : null;
        $this->container['task_type_id'] = isset($data['task_type_id']) ? $data['task_type_id'] : null;
        $this->container['customer_id'] = isset($data['customer_id']) ? $data['customer_id'] : null;
        $this->container['request_id'] = isset($data['request_id']) ? $data['request_id'] : null;
        $this->container['order_id'] = isset($data['order_id']) ? $data['order_id'] : null;
        $this->container['task_step_id'] = isset($data['task_step_id']) ? $data['task_step_id'] : null;
        $this->container['created_date'] = isset($data['created_date']) ? $data['created_date'] : null;
        $this->container['fields'] = isset($data['fields']) ? $data['fields'] : null;
        $this->container['links'] = isset($data['links']) ? $data['links'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets ok
     *
     * @return bool
     */
    public function getOk()
    {
        return $this->container['ok'];
    }

    /**
     * Sets ok
     *
     * @param bool $ok ok
     *
     * @return $this
     */
    public function setOk($ok)
    {
        $this->container['ok'] = $ok;

        return $this;
    }

    /**
     * Gets task_id
     *
     * @return int
     */
    public function getTaskId()
    {
        return $this->container['task_id'];
    }

    /**
     * Sets task_id
     *
     * @param int $task_id task_id
     *
     * @return $this
     */
    public function setTaskId($task_id)
    {
        $this->container['task_id'] = $task_id;

        return $this;
    }

    /**
     * Gets subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->container['subject'];
    }

    /**
     * Sets subject
     *
     * @param string $subject subject
     *
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->container['subject'] = $subject;

        return $this;
    }

    /**
     * Gets message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->container['message'];
    }

    /**
     * Sets message
     *
     * @param string $message message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->container['message'] = $message;

        return $this;
    }

    /**
     * Gets complete
     *
     * @return bool
     */
    public function getComplete()
    {
        return $this->container['complete'];
    }

    /**
     * Sets complete
     *
     * @param bool $complete complete
     *
     * @return $this
     */
    public function setComplete($complete)
    {
        $this->container['complete'] = $complete;

        return $this;
    }

    /**
     * Gets canceled
     *
     * @return bool
     */
    public function getCanceled()
    {
        return $this->container['canceled'];
    }

    /**
     * Sets canceled
     *
     * @param bool $canceled canceled
     *
     * @return $this
     */
    public function setCanceled($canceled)
    {
        $this->container['canceled'] = $canceled;

        return $this;
    }

    /**
     * Gets assigned_to
     *
     * @return int
     */
    public function getAssignedTo()
    {
        return $this->container['assigned_to'];
    }

    /**
     * Sets assigned_to
     *
     * @param int $assigned_to assigned_to
     *
     * @return $this
     */
    public function setAssignedTo($assigned_to)
    {
        $this->container['assigned_to'] = $assigned_to;

        return $this;
    }

    /**
     * Gets group_assigned_to
     *
     * @return int
     */
    public function getGroupAssignedTo()
    {
        return $this->container['group_assigned_to'];
    }

    /**
     * Sets group_assigned_to
     *
     * @param int $group_assigned_to group_assigned_to
     *
     * @return $this
     */
    public function setGroupAssignedTo($group_assigned_to)
    {
        $this->container['group_assigned_to'] = $group_assigned_to;

        return $this;
    }

    /**
     * Gets task_type_id
     *
     * @return int
     */
    public function getTaskTypeId()
    {
        return $this->container['task_type_id'];
    }

    /**
     * Sets task_type_id
     *
     * @param int $task_type_id task_type_id
     *
     * @return $this
     */
    public function setTaskTypeId($task_type_id)
    {
        $this->container['task_type_id'] = $task_type_id;

        return $this;
    }

    /**
     * Gets customer_id
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->container['customer_id'];
    }

    /**
     * Sets customer_id
     *
     * @param int $customer_id customer_id
     *
     * @return $this
     */
    public function setCustomerId($customer_id)
    {
        $this->container['customer_id'] = $customer_id;

        return $this;
    }

    /**
     * Gets request_id
     *
     * @return int
     */
    public function getRequestId()
    {
        return $this->container['request_id'];
    }

    /**
     * Sets request_id
     *
     * @param int $request_id request_id
     *
     * @return $this
     */
    public function setRequestId($request_id)
    {
        $this->container['request_id'] = $request_id;

        return $this;
    }

    /**
     * Gets order_id
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->container['order_id'];
    }

    /**
     * Sets order_id
     *
     * @param int $order_id order_id
     *
     * @return $this
     */
    public function setOrderId($order_id)
    {
        $this->container['order_id'] = $order_id;

        return $this;
    }

    /**
     * Gets task_step_id
     *
     * @return int
     */
    public function getTaskStepId()
    {
        return $this->container['task_step_id'];
    }

    /**
     * Sets task_step_id
     *
     * @param int $task_step_id task_step_id
     *
     * @return $this
     */
    public function setTaskStepId($task_step_id)
    {
        $this->container['task_step_id'] = $task_step_id;

        return $this;
    }

    /**
     * Gets created_date
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->container['created_date'];
    }

    /**
     * Sets created_date
     *
     * @param \DateTime $created_date created_date
     *
     * @return $this
     */
    public function setCreatedDate($created_date)
    {
        $this->container['created_date'] = $created_date;

        return $this;
    }

    /**
     * Gets fields
     *
     * @return \vtgus\RevIO\Model\Field[]
     */
    public function getFields()
    {
        return $this->container['fields'];
    }

    /**
     * Sets fields
     *
     * @param \vtgus\RevIO\Model\Field[] $fields fields
     *
     * @return $this
     */
    public function setFields($fields)
    {
        $this->container['fields'] = $fields;

        return $this;
    }

    /**
     * Gets links
     *
     * @return \vtgus\RevIO\Model\Link[]
     */
    public function getLinks()
    {
        return $this->container['links'];
    }

    /**
     * Sets links
     *
     * @param \vtgus\RevIO\Model\Link[] $links links
     *
     * @return $this
     */
    public function setLinks($links)
    {
        $this->container['links'] = $links;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


